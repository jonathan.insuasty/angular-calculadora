import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import {MatSnackBar} from '@angular/material/snack-bar';
import { ServiciosService } from '../services/servicios.service';

@Component({
  selector: 'app-worked-hours',
  templateUrl: './worked-hours.component.html',
  styleUrls: ['./worked-hours.component.css']
})

export class WorkedHoursComponent implements OnInit {


  private urlapi = 'http://3.138.137.142:8080';
  workedHours : any;
  formulario : FormGroup;
  show : string = "none";

  constructor(private serviciosService:ServiciosService, private _snackBar: MatSnackBar) {
    this.formulario = new FormGroup({
      idtecnico : new FormControl(''),
      week : new FormControl('')
    });
  }

  ngOnInit(): void {
  }

  async onSubmit(){
    this.serviciosService.getWorkedHours(this.formulario.value.idtecnico, this.formulario.value.week).then(data => (this.workedHours = data)).catch(error => console.log(error));;
    this.show = "block";
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import {ReportServiceFormComponent} from '../report-service-form/report-service-form.component'
import {WorkedHoursComponent} from '../worked-hours/worked-hours.component'


const routes: Routes = [
  { path: 'reportService', component: ReportServiceFormComponent},
  { path: 'workedHours', component: WorkedHoursComponent},
  { path: '', redirectTo: '/reportService', pathMatch: 'full' }
];


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class RoutingModule { }

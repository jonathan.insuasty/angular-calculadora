import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ServiciosService {
  
  urlapi:string;

  constructor(private httpClient:HttpClient) {
    this.urlapi = 'http://localhost:8080';
  }

  getAll(): Promise<any[]> {
    return this.httpClient.get<any[]>(`${this.urlapi}/servicios`).toPromise();
  }

  addReport({idtecnico, idservicio, fechainicio, fechafin}) : Promise<any> {
    const bodyRequest = {idtecnico, idservicio, fechainicio, fechafin};
    return this.httpClient.post<any>(`${this.urlapi}/reporteServicio`, bodyRequest).toPromise();
  }

  getWorkedHours(idtecnico : String, week : String) {
    const url = `${this.urlapi}/reporteServicio?idtecnico=${idtecnico}&week=${week}`;
    return this.httpClient.get(url).toPromise();
  }

}

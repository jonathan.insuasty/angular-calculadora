import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { ServiciosService } from './servicios.service';


describe('ServiciosService', () => {
  let service: ServiciosService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(ServiciosService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  it('should retrieve a list of services with almost 1 item', () => {

    service.getAll().then(data => {
      console.log(data);
      expect(data.length).toBeGreaterThan(0);
    });

    const rq = httpTestingController.expectOne({ method: 'GET', url: 'http://localhost:8080/servicios' });
    expect(rq.request.method).toBe('GET');

    rq.flush([
      {
          idservicio: 101,
          descripcion: "Reparación de lavadora"
      },
      {
          idservicio: 102,
          descripcion: "Reparación de puerta atascada"
      },
      {
          idservicio: 103,
          descripcion: "Reparación de fuga de agua en lavamanos"
      }
    ]);
  
  });
});

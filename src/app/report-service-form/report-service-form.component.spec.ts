import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { AppModule } from '../app.module';
import { ReportServiceFormComponent } from './report-service-form.component';



describe('ReportServiceFormComponent', () => {
  let component: ReportServiceFormComponent;
  let fixture: ComponentFixture<ReportServiceFormComponent>;
  let element: DebugElement;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppModule]
    })
    .compileComponents().then(() => {
      fixture = TestBed.createComponent(ReportServiceFormComponent);
      component = fixture.componentInstance;
      element = fixture.debugElement;
    });
  }));

  it('Should appear more than 0 service tag', () => {
    expect(component).toBeTruthy();
    component.services = [
      {
          idservicio: 101,
          descripcion: "Reparación de lavadora"
      },
      {
          idservicio: 102,
          descripcion: "Reparación de puerta atascada"
      },
      {
          idservicio: 103,
          descripcion: "Reparación de fuga de agua en lavamanos"
      }
    ];
    fixture.detectChanges();
    let ser = element.queryAll(By.css('.servicio'));
    expect(ser.length).toBeGreaterThan(0);
  });
});
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';
import { ServiciosService } from '../services/servicios.service';

@Component({
  selector: 'app-report-service-form',
  templateUrl: './report-service-form.component.html',
  styleUrls: ['./report-service-form.component.css']
})
export class ReportServiceFormComponent implements OnInit {

  public services: any[] = null;
  formulario : FormGroup;


  constructor(private serviciosService:ServiciosService, private _snackBar:MatSnackBar) { 
    this.formulario = new FormGroup({
      idtecnico : new FormControl(''),
      idservicio : new FormControl(''),
      fechainicio : new FormControl(''),
      fechafin : new FormControl('')
    });

  }

  ngOnInit(): void {
    this.serviciosService.getAll().then(data => (this.services = data)).catch(error => console.log(error));
  }

  async onSubmit() {
    try {
      const response = await this.serviciosService.addReport(this.formulario.value);
      this._snackBar.open("El servicio fue registrado con exito!", "", {
        duration: 3000,
        panelClass: ['success'],
      });
      this.formulario.reset();
    } catch(error) {
      this._snackBar.open(error.error.error, "", {
        duration: 4000,
        panelClass: ['error'],
      });
      console.log(error);
    }
  }

}
